FROM nginx
LABEL MAINTAINER="<jonas.cavalcantineto@funceme.com>"
RUN apt-get update
RUN apt-get install -y 	apt-utils \
						curl \
						gnupg \
						procps \
						supervisor \
						git \
						build-essential \
						perl


ADD confs/supervisord.conf /etc/supervisord.conf
ADD confs/default.conf /etc/nginx/conf.d/default.conf

RUN set -ex \
	&& curl -sL https://deb.nodesource.com/setup_10.x | bash - \
	&& apt-get install -y nodejs \
	#&& npm config set registry http://nexus.funceme.br/repository/npm-proxy/ \
	&& npm i npm@latest -g \
	&& npm install -g @angular/cli

ADD confs/start_angular.sh /start_angular.sh
RUN chmod +x /start_angular.sh

ADD confs/start.sh /start.sh
RUN chmod +x /start.sh

EXPOSE 80 443

WORKDIR /usr/share/nginx/html/
CMD ["/start.sh"]
